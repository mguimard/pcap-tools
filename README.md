# Pcap tools

Utilities around pcapng files

## Prerequisite

NodeJS

## Usage

Install the needed dependencises

```
npm i
```

Extract frames to text :

```
./parse-pcapng-lite /path/to/file.pcapng <frame length> <filter>

# example:
./parse-pcapng-lite ~/my-capture.pcapng 8 '^c4'
```

